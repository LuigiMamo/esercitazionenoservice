package com.esercitazione.Esercitazione.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.esercitazione.Esercitazione.exception.ResourceNotFoundException;
import com.esercitazione.Esercitazione.models.Employee;
import com.esercitazione.Esercitazione.repositories.EmployeeRepository;

@RestController 
@CrossOrigin(origins="*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeerepository;
	
	@PostMapping("/employees")
	public Employee create (@Valid @RequestBody Employee employee) {
		return employeerepository.save(employee);
	}
	
	@GetMapping("/employees")
	public List<Employee> findAll(){
		return employeerepository.findAll();
	}
	
	@GetMapping("/employees/{id}")
	public Optional<Employee> findById(@PathVariable Long id){
		return employeerepository.findById(id);
	}
	
	@PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable Long id,
         @Valid @RequestBody Employee employeeDetails) throws ResourceNotFoundException {
        Employee employee = employeerepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

        employee.setEmailId(employeeDetails.getEmailId());
        employee.setLastName(employeeDetails.getLastName());
        employee.setFirstName(employeeDetails.getFirstName());
        final Employee updatedEmployee = employeerepository.save(employee);
        return ResponseEntity.ok(updatedEmployee);
    }
	
	@DeleteMapping("/employees/{id}")
	public Map<String, Boolean> delete(@PathVariable Long id) 
		throws ResourceNotFoundException{
			Employee employee = employeerepository.findById(id)
			.orElseThrow(() -> new ResourceNotFoundException("Non trovato per questo id :: " + id ));
			
			employeerepository.delete(employee);
			Map<String, Boolean> response = new HashMap<>();
			response.put("deleted", true);
			return response;
			
		
		
	}
}
