package com.esercitazione.Esercitazione.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.esercitazione.Esercitazione.models.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{}
